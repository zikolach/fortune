package com.proglet.fortune;

import org.robovm.apple.dispatch.DispatchQueue;
import org.robovm.apple.uikit.UITextView;
import org.robovm.apple.uikit.UIViewController;
import org.robovm.objc.annotation.CustomClass;
import org.robovm.objc.annotation.IBAction;
import org.robovm.objc.annotation.IBOutlet;

@CustomClass("MyViewController")
public class MyViewController extends UIViewController {
    private static FortuneClient fortuneClient = new FortuneClient();

    private UITextView fortuneTextView;

    @IBOutlet
    public void setFortuneTextView(UITextView fortuneTextView) {
        this.fortuneTextView = fortuneTextView;
    }

    @IBAction
    private void clicked() {
        fortuneClient.getFortune(this::setFortune);
    }


    private void setFortune(String fortune) {
        DispatchQueue.getMainQueue().sync(() -> fortuneTextView.setText(fortune));
    }
}
