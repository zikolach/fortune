package com.proglet.fortune;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;

public class FortuneActivity extends Activity {
    private FortuneClient fortuneClient = new FortuneClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        final TextView fortuneTextView = (TextView) findViewById(R.id.fortuneTextView);
        final Button nextFortuneButton = (Button) findViewById(R.id.nextFortuneButton);
        final Button monitoringButton = (Button) findViewById(R.id.monitoringButton);

        fortuneTextView.setMovementMethod(new ScrollingMovementMethod());
        nextFortuneButton.setOnClickListener(view -> fortuneClient.getFortune(fortuneTextView::setText));
        monitoringButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, MonitoringActivity.class);
            startActivity(intent);
        });
    }
}
