package com.proglet.fortune;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.widget.TextView;
import org.altbeacon.beacon.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MonitoringActivity extends Activity implements BeaconConsumer {
    private BeaconManager beaconManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranging);
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        final TextView beaconsList = (TextView) findViewById(R.id.beaconsList);

        class BeaconState {
            public Integer distanceInCm;
            public long lastSeen;
        }
        Map<String, BeaconState> beaconsMap = new HashMap<>();

        beaconManager.setRangeNotifier((beacons, region) -> {
            String beaconsListText;
            long current = new Date().getTime();
            // remove too old
            for (Iterator<Map.Entry<String, BeaconState>> it = beaconsMap.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, BeaconState> entry = it.next();
                if (entry.getValue().lastSeen < current - 10000) {
                    it.remove();
                }
            }
            // update
            if (beacons.size() > 0) {
                for (Beacon beacon : beacons) {
                    String id = String.format("%s : %d : %d",
                            String.valueOf(beacon.getId1().toUuid()),
                            beacon.getId2().toInt(),
                            beacon.getId3().toInt());
                    int distanceInCm = (int) (beacon.getDistance() * 100);
                    if (!beaconsMap.containsKey(id)) {
                        beaconsMap.put(id, new BeaconState());
                    }
                    BeaconState bs = beaconsMap.get(id);
                    bs.lastSeen = current;
                    bs.distanceInCm = distanceInCm;
//                    bs.distanceInCm = bs.distanceInCm == null ? distanceInCm :
//                            (int) (bs.distanceInCm * 0.9 + distanceInCm * 0.1);
                }
            }
            if (beaconsMap.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (Map.Entry<String, BeaconState> entry : beaconsMap.entrySet()) {
                    sb
                            .append(entry.getKey())
                            .append(" - ")
                            .append(entry.getValue().distanceInCm)
                            .append("cm")
                            .append("\n");
                }
                beaconsListText = sb.toString();
            } else {
                beaconsListText = "No beacons found";
            }
            runOnUiThread(() -> beaconsList.setText(beaconsListText));
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException ignored) {
        }
    }
}
